public interface Dao<DOMAIN, ID> {
    DOMAIN findById(ID id);
    void save (DOMAIN domain);
    void update (DOMAIN domain);
    void delete (DOMAIN domain);
}
