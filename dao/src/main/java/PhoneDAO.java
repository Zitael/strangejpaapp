import model.Phone;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class PhoneDAO implements Dao<Phone, Long>{
    @Override
    public Phone findById(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Phone phone = session.get(Phone.class, id);
        Hibernate.initialize(phone);
        session.close();
        return phone;
    }

    @Override
    public void save(Phone phone) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.saveOrUpdate(phone);
        tx1.commit();
        session.close();
    }

    @Override
    public void update(Phone phone) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(phone);
        tx1.commit();
        session.close();
    }

    @Override
    public void delete(Phone phone) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(phone);
        tx1.commit();
        session.close();
    }
}
