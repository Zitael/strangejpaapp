import model.Passport;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class PassportDAO implements Dao<Passport, Long> {
    @Override
    public Passport findById(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Passport passport = session.get(Passport.class, id);
        Hibernate.initialize(passport);
        session.close();
        return passport;
    }

    @Override
    public void save(Passport passport) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.saveOrUpdate(passport);
        tx1.commit();
        session.close();
    }

    @Override
    public void update(Passport passport) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(passport);
        tx1.commit();
        session.close();
    }

    @Override
    public void delete(Passport passport) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(passport);
        tx1.commit();
        session.close();
    }
}
