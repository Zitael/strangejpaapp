import model.Phone;

public class PhoneService {
    private PhoneDAO phoneDAO = new PhoneDAO();

    public Phone findById(Long id) {
        return phoneDAO.findById(id);
    }

    public void save(Phone phone) {
        phoneDAO.save(phone);
    }

    public void update(Phone phone) {
        phoneDAO.update(phone);
    }

    public void delete(Phone phone) {
        phoneDAO.delete(phone);
    }
}
