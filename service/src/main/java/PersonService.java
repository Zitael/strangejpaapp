import model.Person;

public class PersonService {
    private PersonDAO personDAO = new PersonDAO();

    public Person findById(Long id) {
        return personDAO.findById(id);
    }

    public void save(Person person) {
        personDAO.save(person);
    }

    public void update(Person person) {
        personDAO.update(person);
    }

    public void delete(Person person) {
        personDAO.delete(person);
    }
}
