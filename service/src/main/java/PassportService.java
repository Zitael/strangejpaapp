import model.Passport;

public class PassportService {
    private PassportDAO passportDAO = new PassportDAO();

    public Passport findById(Long id) {
        return passportDAO.findById(id);
    }

    public void save(Passport passport) {
        passportDAO.save(passport);
    }

    public void update(Passport passport) {
        passportDAO.update(passport);
    }

    public void delete(Passport passport) {
        passportDAO.delete(passport);
    }
}
