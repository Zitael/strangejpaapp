import model.Passport;
import model.Person;
import model.Phone;

import java.util.Collections;

public class Runner {
    public static void main(String[] args) {
        PersonService personService = new PersonService();
        PhoneService phoneService = new PhoneService();
        PassportService passportService = new PassportService();
        System.out.println("Начало, сохранение в базу");
        Person person = new Person();
        person.setName("Max");

        Phone phone = new Phone();
        phone.setPhoneNumber(8887766);
        phoneService.save(phone);
        person.setPhones(Collections.singletonList(phone));

        Passport passport = new Passport();
        passport.setPassportNumber(1111);
        System.out.println("Добавляем паспорт");
        passportService.save(passport);
        System.out.println("Успешно, id = " + passport.getId());

        person.setPassport(passport);
        personService.save(person);
        long personId = person.getId();
        System.out.println("Успешно, id = "+ person.getId());
        System.out.println("Имя:"  + person.getName());
        System.out.println("Номер паспорта: " + person.getPassport().getPassportNumber());
        for (Phone ph: person.getPhones()){
            System.out.println("Телефон: "+ ph.getPhoneNumber());
        }        System.out.println("============================");
        System.out.println("Читаем из базы:");
        Person currentPerson = personService.findById(personId);
        if (currentPerson != null){
            System.out.println("Найдена запись с  id = " + currentPerson.getId());
            System.out.println("Имя:"  + currentPerson.getName());
            System.out.println("Номер паспорта: " + currentPerson.getPassport().getPassportNumber());
            for (Phone ph: currentPerson.getPhones()){
                System.out.println("Телефон: "+ ph.getPhoneNumber());
            }
        } else System.out.println("Ошибка чтения из базы");
        System.out.println("============================");
        System.out.println("Меняем имя у записи");
        currentPerson = personService.findById(personId);
        currentPerson.setName("Ivan");
        personService.update(currentPerson);
        currentPerson = personService.findById(personId);
        System.out.println("Новое имя: " + currentPerson.getName());
        System.out.println("============================");
        System.out.println("Удаление из базы");
        personService.delete(currentPerson);
        currentPerson = personService.findById(personId);
        System.out.println("Поиск по id = 1:" +currentPerson);
    }
}
